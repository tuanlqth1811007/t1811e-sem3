﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
namespace WeatherAppDemo {
    public class LocationData {
        public async static Task<Geoposition> getPosition() {
            var accessStatus = await Geolocator.RequestAccessAsync();
            if (accessStatus != GeolocationAccessStatus.Allowed) throw new Exception();
            var geolocator = new Geolocator { DesiredAccuracy = 0 };
            var position = await geolocator.GetGeopositionAsync();
            return position;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookClientConsumer.ServiceReference1;

namespace BookClientConsumer.Models
{
    public class BookServiceClient
    {
        BookServicesClient client = new BookServicesClient();

        public List<Book> getAllBook()
        {
            var list = client.GetBookList().ToList();
            var rt = new List<Book>();
            list.ForEach(b => rt.Add(new Book()
            {
                BookId = b.BookID,
                ISBN = b.ISBN,
                Title = b.Title
            }
            ));
            return rt;
        }

        public string AddBook(Book newBook)
        {
            var book = new ServiceReference1.Book()
            {
                BookID = newBook.BookId,
                ISBN = newBook.ISBN,
                Title = newBook.Title
            };
            return client.AddBook(book);
        }

        public string EditBook(Book updateBook)
        {
            var book = new ServiceReference1.Book()
            {
                BookID = updateBook.BookId,
                ISBN = updateBook.ISBN,
                Title = updateBook.Title
            };
            return client.UpdateBook(book);
        }

        public string DeleteBook(int id)
        {
            return client.DeleteBook(Convert.ToString(id));
        }
    }
}
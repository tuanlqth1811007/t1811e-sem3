﻿using BookClientConsumer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookClientConsumer.Controllers
{
    public class BookController : Controller
    {
        BookServiceClient bookService = new BookServiceClient();
        // GET: Book
        public ActionResult Index()
        {
            ViewBag.listBook = bookService.getAllBook();
            return View(bookService.getAllBook());
        }

        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            var book = bookService.getAllBook().Where(b => b.BookId == id).FirstOrDefault();
            return View(book);
        }

        // GET: Book/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Book/Create
        [HttpPost]
        public ActionResult Create(Book book)
        {
            try
            {
                bookService.AddBook(book);
                return RedirectToAction("Index", "Book");
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Edit/5
        public ActionResult Edit(int id)
        {
            var book = bookService.getAllBook().Where(b => b.BookId == id).FirstOrDefault();
            return View(book);
        }

        // POST: Book/Edit/5
        [HttpPost]
        public ActionResult Edit(Book book)
        {
            bookService.EditBook(book);
            return RedirectToAction("Index", "Book");
        }

        // GET: Book/Delete/5
        public ActionResult Delete(int id)
        {
            var book = bookService.getAllBook().Where(b => b.BookId == id).FirstOrDefault();
            return View(book);
        }

        // POST: Book/Delete/5
        [HttpPost]
        public ActionResult Delete(string id)
        {
            bookService.DeleteBook(int.Parse(id));
            return RedirectToAction("Index", "Book");
        }
    }
}

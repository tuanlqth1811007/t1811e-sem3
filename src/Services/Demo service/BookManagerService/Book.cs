﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BookManagerService
{
    [DataContract]
    public class Book   
    {
        [DataMember]
        public int BookID { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ISBN { get; set; }
    }

    public interface IBookRepository
    {
        List<Book> GetAllBook();
        Book GetBookById(int id);
        Book AddNewBook(Book item);
        bool DeleteABook(int id);
        bool UpdateABook(Book item);
    }

    public class Repository : IBookRepository
    {
        private List<Book> books = new List<Book>();
        private int counter = 1;
        public Repository()
        {
            AddNewBook(new Book() { Title = "Java", ISBN = "01" });
            AddNewBook(new Book() { Title = "C#", ISBN = "02" });
            AddNewBook(new Book() { Title = "PHP", ISBN = "03" });
        }
        public Book AddNewBook(Book item)
        {
            if(item == null)
            {
                throw new AggregateException("newBook");
            }
            item.BookID = counter;
            books.Add(item);
            counter++;
            return item;
        }

        public bool DeleteABook(int id)
        {
            int idx = books.FindIndex(b => b.BookID == id);
            if(idx == -1)
            {
                return false;
            }
            books.RemoveAll(b => b.BookID == id);
            return true;
        }

        public List<Book> GetAllBook()
        {
            return books;
        }

        public Book GetBookById(int id)
        {
            return books.Find(b => b.BookID == id);
        }

        public bool UpdateABook(Book item)
        {
            int idx = books.FindIndex(b => b.BookID == item.BookID);
            if (idx == -1)
            {
                return false;
            }
            books.RemoveAt(idx);
            books.Add(item);
            return true;
        }
    }
}
﻿using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestPaypal
{
    public class Configuration
    {
        public readonly static string cliendId;
        public readonly static string clientSecret;

        static Configuration()
        {
            var config = getconfig();
            cliendId = "ASh2G5pYskbJ7LWpcXrR0PFuOqr9yj-vajN1WXSZhcST_uhnpbowvKF9HXzjNIsPrYmo7OnIv-ztKpsG";
            clientSecret = "EJchE6ozu0fNvcFgaDFzs5dho93u9Fgg0oQjLGyqfd5kuNKDGLCYmoNFPTlV1ZBLP2r7H__P5oJuyBTc";
        }

        private static Dictionary<string, string> getconfig()
        {
            return PayPal.Api.ConfigManager.Instance.GetProperties();
        }

        private static string GetAccessToken()
        {
            string accessToken = new OAuthTokenCredential(cliendId, clientSecret, getconfig()).GetAccessToken();
            return accessToken;
        }

        public static APIContext GetAPIContext()
        {
            APIContext apiContext = new APIContext(GetAccessToken());
            apiContext.Config = getconfig();
            return apiContext;
        }
    }
}
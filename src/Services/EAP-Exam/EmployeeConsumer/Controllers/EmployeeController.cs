﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EmployeeConsumer.EmployeeService;

namespace EmployeeConsumer.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeServiceClient EmployeeServiceClient = new EmployeeServiceClient("BasicHttpsBinding_IEmployeeService");
        // GET: Employee
        public ActionResult Index()
        {
            List<Employee> employees = EmployeeServiceClient.GetEmployees().ToList();
            return View(employees);
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            try
            {
                // TODO: Add insert logic here
                EmployeeServiceClient.AddEmployee(employee);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //Search
        public ActionResult Search(string departmentName)
        {
            try
            {
                List<Employee> employees = EmployeeServiceClient.SearchEmployee(departmentName).ToList();
                if(employees == null)
                {
                    return RedirectToAction("Index");
                }
                return View("Index", employees);
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EAPAss
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeService.svc or EmployeeService.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeService : IEmployeeService
    {
        EmployeeDataContext dataContext = new EmployeeDataContext();
        public bool AddEmployee(Employee employee)
        {
            try
            {
                dataContext.Employees.InsertOnSubmit(employee);
                dataContext.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<Employee> GetEmployees()
        {
            return dataContext.Employees.ToList();
        }

        public List<Employee> SearchEmployee(string derpartmentName)
        {
            try
            {
                List<Employee> employees = dataContext.Employees.Where(emp => emp.DepartmentName.ToLower().Contains(derpartmentName.ToLower())).ToList();
                if(employees != null)
                {
                    return employees;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }
    }
}

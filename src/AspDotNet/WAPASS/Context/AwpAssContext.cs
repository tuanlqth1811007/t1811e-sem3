﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WAPASS.Models;

namespace WAPASS.Context
{
    public class AwpAssContext : DbContext
    {
        public AwpAssContext() : base("AWPASSContext") {  }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
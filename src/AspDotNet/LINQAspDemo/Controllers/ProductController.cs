﻿using LINQAspDemo.Context;
using LINQAspDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LINQAspDemo.Controllers {
    public class ProductController : Controller {
        ProductContext db = new ProductContext(); //Tạo context kết nối đến cơ sở dữ liệu
        // GET: Product
        public ActionResult Index() {
            var Products = db.Product.ToList();
            return View(Products);
        }

        //Action OrderByName sử dụng LINQ
        public ActionResult OrderByName() {
            var Products = from p in db.Product
                          orderby p.Name ascending
                          select p;
            return View(Products);
        }

        //Action OrderByName sử dụng LINQ
        public ActionResult OrderByPrice() {
            var Products = from p in db.Product
                           orderby p.Price ascending
                           select p;
            return View(Products);
        }

        // GET: Product/Details/5
        public ActionResult Details(int id) {
            return View();
        }

        // GET: Product/Create
        public ActionResult Create() {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(Product product) {
            try {
                // TODO: Add insert logic here
                db.Product.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            } catch {
                return View();
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id) {
            return View();
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection) {
            try {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            } catch {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id) {
            return View();
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection) {
            try {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            } catch {
                return View();
            }
        }
    }
}

﻿using LINQAspDemo.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LINQAspDemo.Context {
    public class ProductContext : DbContext{
        public DbSet<Product> Product { get; set; }
    }
}
﻿using LambdaExpressionsDemo.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LambdaExpressionsDemo.Context {
    public class BookContext : DbContext{
        public DbSet<Book> Books { get; set; }
    }
}
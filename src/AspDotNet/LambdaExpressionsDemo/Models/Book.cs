﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LambdaExpressionsDemo.Models {
    public class Book {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Author { get; set; }
        public decimal Price { get; set; }
        public DateTime Published { get; set; }
    }
}
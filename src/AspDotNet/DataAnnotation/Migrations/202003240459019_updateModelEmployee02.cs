﻿namespace DataAnnotation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateModelEmployee02 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Employees", "Address", c => c.String(nullable: false, maxLength: 300));
            AlterColumn("dbo.Employees", "Email", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Employees", "Email", c => c.String());
            AlterColumn("dbo.Employees", "Address", c => c.String());
        }
    }
}

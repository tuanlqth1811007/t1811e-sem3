﻿using DataAnnotation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DataAnnotation.Context {
    public class EmployeeContext : DbContext {
        public EmployeeContext() : base("DataAnnotationContext") {

        }
        public DbSet<Employee> Employees { get; set; }
    }
}
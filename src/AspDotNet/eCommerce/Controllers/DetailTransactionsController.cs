﻿using eCommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace eCommerce.Controllers
{
    public class DetailTransactionsController : Controller
    {
        private eCommerceEntities db = new eCommerceEntities();
        // GET: DetailTransactions
        public ActionResult Index()
        {
            var transaction = db.ACC_TRANSACTION;
            var transaction_customer = (from t in transaction
                     select new DetailTransaction {
                         FirstName = t.ACCOUNT.CUSTOMER.INDIVIDUAL.FIRST_NAME,
                         LastName = t.ACCOUNT.CUSTOMER.INDIVIDUAL.LAST_NAME,
                         NameProduct = t.ACCOUNT.PRODUCT.NAME,
                         TypeProduct = t.ACCOUNT.PRODUCT.PRODUCT_TYPE.NAME,
                         Brand = t.ACCOUNT.BRANCH.NAME,
                         EmpFirstName = t.ACCOUNT.EMPLOYEE.FIRST_NAME,
                         EmpLastName = t.ACCOUNT.EMPLOYEE.LAST_NAME,
                         CreateAt = t.TXN_DATE
                     });
            ViewBag.DetailTransaction = transaction_customer.ToList();
            return View();
        }

        // GET: DetailTransactions/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: DetailTransactions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DetailTransactions/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DetailTransactions/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DetailTransactions/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DetailTransactions/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DetailTransactions/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

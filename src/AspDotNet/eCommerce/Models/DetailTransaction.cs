﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eCommerce.Models {
    public class DetailTransaction {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NameProduct { get; set; }
        public string TypeProduct { get; set; }
        public string Brand { get; set; }
        public string EmpFirstName { get; set; }
        public string EmpLastName { get; set; }
        public DateTime CreateAt { get; set; }
    }
}
﻿using MyCodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyCodeFirst.DAL {
    public class MyCodeFirstContext : DbContext{
        public MyCodeFirstContext() : base("MyCodeFirstContext") {

        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Course> Courses { get; set; }
    }
}